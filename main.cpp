#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickItem>
#include <QtQuick/QQuickView>
#include <QQmlContext>
#include <QTimer>
#include "CommObject.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Create instance of object with slots and signals
    CommObject co;
    co.setObjectName("commObject");

    // Create view
    QQuickView view;
    // Add reference to object to view's rootContext as property
    view.rootContext()->setContextProperty("commObject", &co);
    // Load QML in view
    view.setSource(QUrl("qrc:/main"));

    // Retrieve top-level object which is a Rectangle
    QObject *rect = dynamic_cast<QObject*>(view.rootObject());
    // Connect QML signal to object's slot
    QObject::connect(rect, SIGNAL(handleText(QString)),
                     &co, SLOT(slotRetrieveText(QString)));

    view.show();

    QTimer timer;
    timer.setInterval(5000);
    co.setText("Text changed from C++");
    QObject::connect(&timer, SIGNAL(timeout()), &co, SLOT(slotTriggerChangeText()));
    timer.start();

    return app.exec();
}
