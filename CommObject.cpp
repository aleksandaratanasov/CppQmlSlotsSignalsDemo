#include "CommObject.h"
#include <QDebug>

CommObject::CommObject(QObject *parent) :
    QObject(parent),
    text("")
{
}

void CommObject::setText(const QString &msg)
{
    this->text = msg;
}

void CommObject::slotTriggerChangeText()
{
    qDebug() << "Triggering change text signal";
    emit changeText(this->text);
}

void CommObject::slotRetrieveText(const QString &msg)
{
    qDebug() << "Received text from QML: \"" << msg << "\"";
}
