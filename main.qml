import QtQuick 2.0

Rectangle {
    id: test
    width:  200
    height: 50
    x: 10
    y: 10
    signal handleText(string msg)

    Text {
        id: textItem
        objectName: "textItem"
        anchors.centerIn: test
        text: "Text set in QML"

        Connections {
            target: commObject
            onChangeText: {
                textItem.text = newText;
            }
        }
    }

    MouseArea {
        hoverEnabled: false
        anchors.fill: parent
        onClicked: {
            test.handleText(textItem.text)
        }
    }
}
