#ifndef COMMOBJECT_H
#define COMMOBJECT_H

#include <QObject>

class CommObject : public QObject
{
    Q_OBJECT
public:
    explicit CommObject(QObject *parent = Q_NULLPTR);
    void setText(const QString &msg);

signals:
    void changeText(const QString& newText);

public slots:
    void slotTriggerChangeText();
    void slotRetrieveText(const QString &msg);

private:
    QString text;
};

#endif // COMMOBJECT_H
