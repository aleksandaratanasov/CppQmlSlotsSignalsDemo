TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    CommObject.cpp


HEADERS += \
    CommObject.h

#OTHER_FILES += \
#    main.qml

RESOURCES += \
    resources.qrc
